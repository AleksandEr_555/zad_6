<?php
$array[0] = "- Метод GET отправляет скрипту всю собранную информацию формы как часть URL;";
$array[1] = "- Метод POST передает данные таким образом, что пользователь сайта уже не видит передаваемые скрипту данные;";
$array[2] = "- Принцип работы метода GET ограничивает объём передаваемой скрипту информации;";
$array[3] = "- Так как метод GET отправляет скрипту всю собранную информацию формы как часть URL (то есть в открытом виде), то это может пагубно повлиять на безопасность сайта;";
$array[4] = "- Страницу, сгенерированную методом GET, можно пометить закладкой (адрес страницы будет всегда уникальный), а страницу, сгенерированную метод POST нельзя (адрес страницы остается неизменным, так как данные в URL не подставляются);";
$array[5] = "- Используя метод GET можно передавать данные не через веб-форму, а через URL страницы, введя необходимые значения через знак &;";
$array[6] = "- Метод POST в отличие от метода GET позволяет передавать запросу файлы;";
$array[7] = "- При использовании метода GET существует риск того, что поисковый робот может выполнить тот или иной открытый запрос.";
?>

<html>
<body>
	<center>
		<div style='width: 700px; height: 100px; margin-top: 10px; border: 2px solid black; background-color: orange'>
			<h1><b>Домашнее задание №6</b></h1>	
		</div>
		<div style='width: 700px; height: 800px; margin-top: 10px; border: 2px solid black; background-color: #F0E68C'>
			<h3><b>Различия между методами GET и POST</b></h3>
			<br>
			<div align="left" style='padding-left: 25px; padding-right: 10px'>
				<?php
					foreach ($array as $value) 
					{
						echo "<p>$value</p>";
					}
				?>
			</div>
			<hr>
			<h3><b>GET запрос</b></h3>
			<br>
			<form action="content_get.php">
				<input type="text" name="name">
				<input type="text" name="surname">
				<input type="int" name="year">
				<input type="submit">
			</form>
			<hr>
			<h3><b>POST запрос</b></h3>
			<br>
			<form action="content_post.php" method="post">
				<input type="text" name="name">
				<input type="text" name="surname">
				<input type="int" name="year">
				<input type="submit">
			</form>
			<hr>
		</div>
	</center>
</body>
</html>